# Airflow Spark

This project contains the following containers:

* postgres: Postgres database for Airflow metadata and a Test database to test whatever you want.
    * Image: postgres:9.6
    * Database Port: 5432
    * References: https://hub.docker.com/_/postgres

* airflow-webserver: Airflow webserver and Scheduler.

    * Port: 8080

### Clone project

    git clone https://gitlab.com/karthikmtrs/refinitiv_de.git

### Build airflow Docker

    $ docker-compose up

    If you want to run in background:

    $ docker-compose up -d

### Check if you can access

Airflow: http://localhost:8080

